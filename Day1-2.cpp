//Fun/AdventCalendar2020/Day1-2.cpp
//Matthew Ellison
// Created: 12-02-20
//Modified: 12-02-20
//Find the three entries that sum to 2020 and then multiply those three numbers together


#include <iostream>
#include <vector>
#include <algorithm>
#include "Stopwatch.hpp"


std::vector<int> numbersList = {1711, 1924, 1384, 1590, 1876, 1918, 2003, 1514, 1608, 1984, 1706, 1375, 1476, 1909, 1615, 1879, 1940, 1945, 1899, 1510,
								1657, 1685, 1588, 1884, 1864, 1995, 1648, 1713, 1532, 1556, 1572, 1667, 1861, 1773, 1501, 1564, 1756,  395, 1585, 1717,
								1553, 1487, 1617, 1808, 1780, 1570, 1881, 1992, 1894, 1772, 1837, 2002, 1659, 1731, 1873, 1760,  552, 1575, 1597, 1986,
								1416, 1398, 1737, 1027, 1457,  198, 1904, 1753, 1727,  633, 1577, 1944, 1369, 1400, 1843, 1966, 1008, 1681, 1890, 1939,
								1605, 1548, 1953, 1839, 1409, 1592, 1744, 1761, 1613, 1412, 1759,  703, 1498, 1941, 1425, 1528, 1469, 1728, 1447, 1406,
								1797, 1543, 1682, 1722, 1723, 1893, 1644,  796, 1505, 1715, 1729, 1943, 1626, 1602, 1964, 1509, 1816, 1660, 1399, 1996,
								1750, 1701, 1963, 1979, 1558, 1506, 1465, 2001, 1935, 1616, 1990, 1946, 1818, 1892, 1431, 1832, 1688, 2004, 1424, 1716,
								1897, 1931, 1557, 1389, 1872, 1640, 1670, 1911, 1427, 1730,  211, 1420, 1488, 1689, 1383, 1967, 1594,  642, 1622, 1627,
								1607, 1372, 1596, 1451, 1693, 1380, 1745, 1908, 1785, 1646, 1824, 1418, 1258, 1664, 1631, 1459, 1901, 1838, 1794, 1815,
								1388, 1809, 1920, 1411, 1593, 1676, 1610, 1629, 1512, 1522, 1649, 1740, 1695, 1504, 1856, 1791, 1898, 1661, 1806, 1851};
int NEEDED_SUM = 2020;


int main(){
	int firstNum = 0;	//Holds the first number of the sum
	int secondNum = 0;	//Holds the second number of the sum
	int thirdNum = 0;	//Holds the third number of the sum
	bool found = false;	//Flags whether the answer has already been found
	mee::Stopwatch timer;

	//Start the timer
	timer.start();

	std::sort(numbersList.begin(), numbersList.end());

	//Find the two numbers in the array that make the correct sum
	for(int cnt1 = 0;(cnt1 < numbersList.size()) && (!found);++cnt1){
		for(int cnt2 = cnt1 + 1;(cnt2 < numbersList.size()) && (!found);++cnt2){
			for(int cnt3 = cnt2 + 1;(cnt3 < numbersList.size()) && (!found);++cnt3){
				int sum = numbersList[cnt1] + numbersList[cnt2] + numbersList[cnt3];
				if(sum > NEEDED_SUM){
					break;
				}
				else if(sum == NEEDED_SUM){
					firstNum = numbersList[cnt1];
					secondNum = numbersList[cnt2];
					thirdNum = numbersList[cnt3];
					found = true;
				}
			}
		}
	}

	//Get the product of the 2 numbers
	int prod = firstNum * secondNum * thirdNum;

	//Stop the timer
	timer.stop();

	//Print the results
	std::cout << "The numbers who's sum is " << NEEDED_SUM << " are " << firstNum << ", " << secondNum << ", and " << thirdNum
			<< "\nThe product of the numbers is " << prod
			<<"\nIt took " << timer.getStr() << " to finish this problem" << std::endl;

	return 0;
}

/* Results:
The numbers who's sum is 2020 are 395, 198, and 1427
The product of the numbers is 111605670
It took 2.998 milliseconds to finish this problem
*/
