//Fun/AdventCalendar2020/Day2-2.cpp
//Matthew Ellison
// Created: 12-02-20
//Modified: 12-02-20
//In the given pattern '.' represents open space and # represents a tree
//Going in a right 3 down 1 pattern how many trees will you find? (On those specific points, not in between)
//Add to this the patterns of 1,1; 5,1; 7,1; and 1,2


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cinttypes>
#include "Algorithms.hpp"
#include "Stopwatch.hpp"


std::vector<std::vector<char>> map;


void importData(){
	std::ifstream inputFile;
	inputFile.open("inputs/Day3.txt");

	//Loop until you find the end of the file
	while(!inputFile.eof()){
		//Put the next line of the file into a string
		std::string currentLine;
		inputFile >> currentLine;
		//Put every character in the string into a new array slot
		std::vector<char> newVector;
		for(char ch : currentLine){
			newVector.push_back(ch);
		}
		//Put the new vector in the large vector
		map.push_back(newVector);
	}
}

//Starting at 0,0 see how many #'s you find going {right} over and {down} down
int followPattern(int right, int down){
	int column = 0;	//The column on the map we are currently on
	int row = 0;	//The row on the map we are currently on
	int numTrees = 0;	//The number of trees on this path
	while(row < map.size()){
		//When you reach the right side of the array it just repeats so you can % the number
		char ch = map[row][column % map[row].size()];
		if(ch == '#'){
			++numTrees;
		}
		else if(ch == '.'){
			//Do nothing
		}
		else{
			exit(1);
		}
		//Advance the row and column
		row += down;
		column += right;
	}
	//Return the number of trees on the path
	return numTrees;
}

int main(){
	mee::Stopwatch timer;
	std::vector<int64_t> numTrees;
	//Get the data from the file
	importData();

	//Start the timer
	timer.start();


	//Starting at 0,0 see how many #'s you find going 1 over and 1 down
	numTrees.push_back(followPattern(1, 1));
	//Starting at 0,0 see how many #'s you find going 3 over and 1 down
	numTrees.push_back(followPattern(3, 1));
	//Starting at 0,0 see how many #'s you find going 5 over and 1 down
	numTrees.push_back(followPattern(5, 1));
	//Starting at 0,0 see how many #'s you find going 7 over and 1 down
	numTrees.push_back(followPattern(7, 1));
	//Starting at 0,0 see how many #'s you find going 1 over and 2 down
	numTrees.push_back(followPattern(1, 2));


	//Stop the timer
	timer.stop();

	//Print the results
	std::cout << "There are ";
	for(int num : numTrees){
		std::cout << num << ", ";
	}
	std::cout  << " trees on these paths"
			<< "\nThe product of these numbers is " << mee::getProduct(numTrees)
			<< "\nIt took " << timer.getStr() << " to finish this problem" << std::endl;

	return 0;
}

/* Results:
There are 84, 195, 70, 70, 47,  trees on these paths
The product of these numbers is 3772314000
It took 0.000 nanoseconds to finish this problem
*/
