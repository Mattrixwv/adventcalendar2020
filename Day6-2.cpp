//Fun/AdventCalendar2020/Day6-2.cpp
//Matthew Ellison
// Created: 12-06-20
//Modified: 12-06-20
//The sum of the counts of the questions answered yes in each group


#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include "Stopwatch.hpp"
#include "Algorithms.hpp"


bool isFound(std::vector<char> ary, char ch){
	for(char c : ary){
		if(c == ch){
			return true;
		}
	}
	return false;
}
struct Person{
	std::vector<char> yeses;
};
struct Group{
	std::vector<Person> people;
	std::vector<char> getYeses(){
		std::vector<char> yeses;
		//Go through every person in people and gather their yes answers into a single vector
		for(Person per : people){
			for(char ch : per.yeses){
				//Add them to the vector only if they don't already exist
				if(!isFound(yeses, ch)){
					yeses.push_back(ch);
				}
			}
		}
		//Return the vector with all the yes answers
		return yeses;
	}
	std::vector<char> getUniversalYeses(){
		std::vector<char> yeses;
		//You only need to look at the characters for the first person
		for(char ch : people.front().yeses){
			int yesCount = 1;
			//Count how many times the character appears in all of the people
			for(int cnt = 1;cnt < people.size();++cnt){
				if(isFound(people[cnt].yeses, ch)){
					++yesCount;
				}
			}
			//If the character was in everyones "yeses" then add it to the array
			if(yesCount == people.size()){
				yeses.push_back(ch);
			}
		}
		//Return the array of all univeral yeses
		return yeses;
	}
};


std::vector<Group> getInput(){
	std::ifstream inputFile("inputs/Day6.txt");
	std::vector<Group> groups;

	//Go through the entire file
	while(!inputFile.eof()){
		Group group;
		std::string currentLine;
		//Each new line is a new person
		std::getline(inputFile, currentLine);
		//Go through the file until you find an empty line
		//An empty line signifies a new group
		while(!currentLine.empty()){
			Person person;
			//Add all characters to the vector
			for(char ch : currentLine){
				person.yeses.push_back(ch);
			}
			//Add the person to the group
			group.people.push_back(person);
			//Get the next line
			std::getline(inputFile, currentLine);
		}
		//When a blank line is found move to a new group
		groups.push_back(group);
	}

	//Return all of the groups in the file
	return groups;
}


int main(){
	mee::Stopwatch timer;
	std::vector<int> yeses;

	//Get the input from the file
	std::vector<Group> groups = getInput();


	//Start the timer
	timer.start();

	//Go through every group, get the number of yeses, and add it to the sum
	for(Group group : groups){
		yeses.push_back(group.getUniversalYeses().size());
	}
	//Get the sum of the number of yeses in all groups
	int sum = mee::getSum(yeses);


	//Stop the timer
	timer.stop();

	//Print the results
	std::cout << "The sum of all yeses is " << sum
			<< "\nIt took " << timer.getStr() << " to finish this problem" << std::endl;

	return 0;
}

/* Results:
The sum of all yeses is 3356
It took 2.998 milliseconds to finish this problem
*/
